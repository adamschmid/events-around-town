<?php
require_once 'BaseAPI.php';
class GetEventsByDateAPI extends BaseAPI {
	// Main method to redeem a code
	function call() {

		$this->checkToken();
		$query = "
			SELECT
				event.event_name,
				event.id,
				loc.city,
				event.start_time
			FROM gp_event event
			INNER JOIN gp_location loc
			ON event.location_id = loc.id
			ORDER BY start_time ASC;
		";
		$stmt = $this->db->prepare($query);
		$stmt->execute();

		/* bind result variables */
		$stmt->bind_result(
			$event_name,
			$event_id,
			$event_city,
			$event_time
		);
		$rows = array();

		/* fetch values */
		while ($stmt->fetch()) {
			$post = array(
				"name"=>$event_name,
				"id"=>$event_id,
				"city"=>$event_city,
				"time"=>$event_time
			);
			$rows['events'][] = $post;
		}

		$this->sendResponse(200, json_encode($rows));
		$stmt->close();
	}
}

// This is the first thing that gets called when this page is loaded
// Creates a new instance of the RedeemAPI class and calls the redeem method
$api = new GetEventsByDateAPI;
$api->call();
?>