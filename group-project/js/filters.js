$( document ).ready(function() {

	//get categories
	$.get( "../api/GetEventCategoriesAPI.php", function( data ) {
		data.events.forEach(function(gp_cats) {
			$( "#categories" ).append( "<option value="+ gp_cats.category_id +">" + gp_cats.category + "</option>" );
		});
	});

	//submit values from form to event filter api
	$( "#filter-submit" ).submit(function(event) {
		var posting = $.post( "api/FilterEventsAPI.php", {
			category_id: $("#categories").val(),
			zip: $( "#zip" ).val()
		});

		posting.done(function( data ) {
			logged_in = true;
			user_id = data.user_id;
			refreshUser();
		});
		event.preventDefault();
	});

});