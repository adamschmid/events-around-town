var logged_in = false;
var is_admin = false;
var user_id = "";
var selected_event_id = "";

$( document ).ready(function() {
	refreshData();
	refreshUser();

	$("#refresh_button").click(function(event) {
		refreshData();
	});

	$("#login_button").click(function(event) {
		$(".inner").hide();
		// Show the div we want
		$("#login_screen").show();
	});

	$("#createaccount_button").click(function(event) {
		$(".inner").hide();
		// Show the div we want
		$("#createaccount_screen").show();
	});

	$("#add_button").click(function(event) {
		if(logged_in) {
			$(".inner").hide();
			// Show the div we want
			$("#add_event").show();
		} else {
			alert("You must be logged in to add events!");
		}
	});

	// -----------------------------------------------------add editUser_Button
	$("#editUser_button").click(function(event) {
		if(logged_in) {
			$(".inner").hide();
			// Show the div we want
			$("#editUser_screen").show();
		} else {
			alert("You must be logged in to add events!");
		}
	});


	$(".back_button").click(function(event) {
		$(".inner").hide();
		refreshData();
		$("#event_list").show();
	});

	$( "#login-form" ).submit(function(event) {
			var posting = $.post( "api/LoginUserAPI.php", { username: $( "#userLogin" ).val(), password: $( "#userPassword" ).val() } );

			posting.done(function( data ) {
					logged_in = true;
					user_id = data.user_id;
					refreshUser();
			});

			event.preventDefault();
	});

    $( "#registrationForm").submit(function(event) {
            var posting = $.post( "api/CreateUserAPI.php", { username: $( "#createLogin" ).val(), email: $( "#createEmail" ).val(), password: $( "#createPassword" ).val() } );

            posting.done(function( data ) {
                refreshUser();
            });

                event.preventDefault();
    });

	$( "#edituser-form" ).submit(function(event) {
			var edit_data = { username: $( "#edit_username" ).val(), user_id: user_id, password: $( "#edit_password" ).val(), email: $("#edit_email").val() };
			var posting = $.post( "api/editUserAPI.php",  edit_data);

			posting.done(function( data ) {
					refreshUser();
			});

			event.preventDefault();
	});
});

var refreshUser = function() {
	$(".inner").hide();
	refreshData();
	// Show the div we want
	$("#event_list").show();
	if(logged_in) {
		$("#add_button").show();
		$("#editUser_button").show();  // ------------------------------edit User_button show
		$("#login_button").hide();
		$("#createaccount_button").hide();
	}else {
		$("#add_button").hide();
		$("#editUser_button").hide();  // ------------------------------edit User_button hide
		$("#login_button").show();
		$("#createaccount_button").show();
	}
}

var refreshData = function() {
	// Clear existing contents
	$("#event_list_content").html("");
	// Make request for new data
	$.get("mockdata/events.json", function( data ) {
		data.events.forEach(function(gp_event) {
			// TODO: Use switch statement to set correct icon
			var icon_path = "mockdata/icon.png";

			// Start constructing our event div
			var row = "<div class='event_row'><img src='" + icon_path + "' class='icon' /><span class='title_text'>" + gp_event.name + "</span>";

			row += "<button class='detail' data-eventid='" + gp_event.id + "'>More Details</button>";

			if(logged_in) {
				row += "<button class='fav' data-eventid='" + gp_event.id + "'>Favorite</button>";
			}

			if(logged_in && is_admin) {
				row += "<button class='delete' data-eventid='" + gp_event.id + "'>Delete</button>";
				row += "<button class='edit' data-eventid='" + gp_event.id + "'>Edit</button>";
			}

			row += "<br/>" + gp_event.city + "</div>";
			$( "#event_list_content" ).append( row );
			$( "#event_list_content" ).append( "<hr/>" );
		});

		addClickHandlers();
	});
}

// Click handlers must be added after divs have been created or they won't work
var addClickHandlers = function() {
	$(".fav").click(function(event) {
		// Add stared event API call here
		alert("Logged in user stared event with id: " + $(this).data("eventid"));
	});

	$(".delete").click(function(event) {
		//
		alert("Admin deleted event with id: " + $(this).data("eventid"));
	});

	$(".edit").click(function(event) {
		selected_event_id = $(this).data("eventid");
		if(logged_in) {
			$( ".inner" ).hide();
			// Ideally display a spinner
			// Show the div we want
			$( "#edit_event" ).show();
		} else {
			alert("You must be logged in to edit events!");
		}
	});

	$(".detail").click(function(event) {
		showEventDetails($(this).data("eventid"));
	});
}

// Function used to retrieve event details
var showEventDetails = function(event_id) {
	$("#event_detail_content").html("");
	// Hide all inner divs
	$(".inner").hide();
	// Ideally display a spinner
	// Show the div we want
	$("#event_detail").show();

	// Make post request
	var posting = $.post("mockdata/MockAPI.php", { event_id: event_id } );

	// Put the results in a div
	posting.done(function( data ) {
		// Ideally hide spinner here
		$("#event_detail_content").html("Details for event with id " + event_id + " go here.");
	});
}