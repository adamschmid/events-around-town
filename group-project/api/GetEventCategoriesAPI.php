<?php
require_once 'BaseAPI.php';
class GetEventCategoriesAPI extends BaseAPI {
	// Main method to redeem a code
	function call() {
		$this->checkToken();
		$stmt = $this->db->prepare("
			SELECT cats.name, cats.id
			FROM gp_category cats;
		");
		$stmt->execute();

		/* bind result variables */
		$stmt->bind_result(
			$cat_name,
			$cat_id
		);
		$rows = array();

		/* fetch values */
		while ($stmt->fetch()) {
			$post = array(
				"category"=>$cat_name,
				"category_id"=>$cat_id
			);
			$rows['events'][] = $post;
		}

		$this->sendResponse(200, json_encode($rows));
		$stmt->close();
	}
}

// This is the first thing that gets called when this page is loaded
// Creates a new instance of the RedeemAPI class and calls the redeem method
$api = new GetEventCategoriesAPI;
$api->call();
?>