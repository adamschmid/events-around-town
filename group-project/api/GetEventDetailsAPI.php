<?php
require_once 'BaseAPI.php';
class GetEventDetailsAPI extends BaseAPI {
	// Main method to redeem a code
	function call() {

		$id = $_POST['event-id'];

		$this->checkToken();
		$query = "
			SELECT
				event.event_name,
				event.id,
				event.status,
				event.website,
				event.twitter,
				event.cost,
				rating.rating,
				event.start_time,
				event.end_time,
				loc.street,
				loc.city,
				loc.state,
				loc.zip
			FROM gp_event event
			INNER JOIN gp_location loc
			ON event.location_id = loc.id
			INNER JOIN gp_rating rating
			ON loc.id = rating.event_id
			WHERE $id = event.id;
			";
		$stmt = $this->db->prepare($query);
		$stmt->execute();

		/* bind result variables */
		$stmt->bind_result(
			$event_name,
			$event_id,
			$event_status,
			$event_website,
			$event_twitter,
			$event_cost,
			$event_rating,
			$event_startTime,
			$event_endTime,
			$event_address,
			$event_city,
			$event_state,
			$event_zip
		);
		$rows = array();

		/* fetch values */
		while ($stmt->fetch()) {
			$post = array(
				"name"=>$event_name,
				"id"=>$event_id,
				"id"=>$event_status,
				"website"=>$event_website,
				"twitter"=>$event_twitter,
				"cost"=>$event_cost,
				"rating"=>$event_rating,
				"address"=>$event_address,
				"city"=>$event_city,
				"state"=>$event_state,
				"zip"=>$event_zip,
				"start_time"=>$event_startTime,
				"end_time"=>$event_endTime
			);
			$rows['events'][] = $post;
		}

		$this->sendResponse(200, json_encode($rows));
		$stmt->close();
	}
}

// This is the first thing that gets called when this page is loaded
// Creates a new instance of the RedeemAPI class and calls the redeem method
$api = new GetEventDetailsAPI;
$api->call();
?>