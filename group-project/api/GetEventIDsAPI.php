<?php
require_once 'BaseAPI.php';
class GetEventIDsAPI extends BaseAPI {
	// Main method to redeem a code
	function call() {
		$this->checkToken();
		$stmt = $this->db->prepare("
			SELECT event.event_name, event.id
			FROM gp_event event;
		");
		$stmt->execute();

		/* bind result variables */
		$stmt->bind_result($event_name, $event_id);
		$rows = array();

		/* fetch values */
		while ($stmt->fetch()) {
			$post = array(
				"name"=>$event_name,
				"id"=>$event_id
			);
			$rows['events'][] = $post;
		}

		$this->sendResponse(200, json_encode($rows));
		$stmt->close();
	}
}

// This is the first thing that gets called when this page is loaded
// Creates a new instance of the RedeemAPI class and calls the redeem method
$api = new GetEventIDsAPI;
$api->call();
?>