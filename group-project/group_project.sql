-- phpMyAdmin SQL Dump
-- version 4.2.10
-- http://www.phpmyadmin.net
--
-- Host: localhost:8889
-- Generation Time: May 24, 2016 at 01:35 AM
-- Server version: 5.5.38
-- PHP Version: 5.6.2

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `group_project`
--

-- --------------------------------------------------------

--
-- Table structure for table `gp_category`
--

CREATE TABLE `gp_category` (
`id` int(11) NOT NULL,
  `name` text NOT NULL,
  `icon` text NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `gp_category`
--

INSERT INTO `gp_category` (`id`, `name`, `icon`) VALUES
(1, 'general', '');

-- --------------------------------------------------------

--
-- Table structure for table `gp_event`
--

CREATE TABLE `gp_event` (
`id` int(11) NOT NULL,
  `category_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `start_time` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `end_time` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `time_added` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `cost` int(11) NOT NULL DEFAULT '0',
  `location_id` int(11) NOT NULL,
  `event_name` text NOT NULL,
  `status` int(11) NOT NULL DEFAULT '0',
  `website` text,
  `twitter` text
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `gp_event`
--

INSERT INTO `gp_event` (`id`, `category_id`, `user_id`, `start_time`, `end_time`, `time_added`, `cost`, `location_id`, `event_name`, `status`, `website`, `twitter`) VALUES
(1, 1, 1, '2016-08-05 15:00:00', '2016-08-07 17:00:00', '2016-05-23 20:05:16', 0, 1, 'Uptown Art Fair', 0, 'http://www.uptownartfair.com/', NULL),
(2, 1, 1, '2016-05-31 00:00:00', '2016-05-31 03:00:00', '2016-05-23 20:07:56', 0, 1, 'Swing Dance Night', 0, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `gp_location`
--

CREATE TABLE `gp_location` (
`id` int(11) NOT NULL,
  `street` text,
  `city` text NOT NULL,
  `state` text NOT NULL,
  `zip` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `gp_location`
--

INSERT INTO `gp_location` (`id`, `street`, `city`, `state`, `zip`) VALUES
(1, NULL, 'Minneapolis', 'MN', 0),
(2, NULL, 'Minneapolis', 'MN', 55408);

-- --------------------------------------------------------

--
-- Table structure for table `gp_rating`
--

CREATE TABLE `gp_rating` (
  `user_id` int(11) NOT NULL,
  `event_id` int(11) NOT NULL,
  `rating` int(11) NOT NULL,
  `timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `gp_session`
--

CREATE TABLE `gp_session` (
`id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `token` text NOT NULL,
  `expiration` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `gp_user`
--

CREATE TABLE `gp_user` (
`id` int(11) NOT NULL,
  `role` int(11) NOT NULL DEFAULT '2',
  `username` text NOT NULL,
  `password` text NOT NULL,
  `email` text
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `gp_user`
--

INSERT INTO `gp_user` (`id`, `role`, `username`, `password`, `email`) VALUES
(1, 1, 'chris', 'test', NULL);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `gp_category`
--
ALTER TABLE `gp_category`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `gp_event`
--
ALTER TABLE `gp_event`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `gp_location`
--
ALTER TABLE `gp_location`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `gp_rating`
--
ALTER TABLE `gp_rating`
 ADD PRIMARY KEY (`user_id`,`event_id`);

--
-- Indexes for table `gp_session`
--
ALTER TABLE `gp_session`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `gp_user`
--
ALTER TABLE `gp_user`
 ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `gp_category`
--
ALTER TABLE `gp_category`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `gp_event`
--
ALTER TABLE `gp_event`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `gp_location`
--
ALTER TABLE `gp_location`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `gp_session`
--
ALTER TABLE `gp_session`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `gp_user`
--
ALTER TABLE `gp_user`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
