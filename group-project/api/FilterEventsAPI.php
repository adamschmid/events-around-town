<?php
require_once 'BaseAPI.php';
class GetEventDetailsAPI extends BaseAPI {
	// Main method to redeem a code
	function call() {


		$zip = $_POST['zip'];
		$cat_id = $_POST['category'];

		$this->checkToken();
		$query = "
			SELECT
				event.event_name,
				event.id,
				event.status,
				event.website,
				event.twitter,
				event.cost,
				event.start_time,
				event.end_time,
				loc.street,
				loc.city,
				loc.state,
				loc.zip,
				event.category_id
			FROM gp_event event
			INNER JOIN gp_location loc
			ON event.location_id = loc.id
			WHERE $zip = loc.zip
			OR $cat_id = event.category_id;
			";

			//echo $query;

		$stmt = $this->db->prepare($query);
		$stmt->execute();

		/* bind result variables */
		$stmt->bind_result(
			$event_name,
			$event_id,
			$event_status,
			$event_website,
			$event_twitter,
			$event_cost,
			$event_startTime,
			$event_endTime,
			$event_address,
			$event_city,
			$event_state,
			$event_zip,
			$event_cat_id
		);
		$rows = array();

		/* fetch values */
		while ($stmt->fetch()) {
			$post = array(
				"name"=>$event_name,
				"id"=>$event_id,
				"id"=>$event_status,
				"website"=>$event_website,
				"twitter"=>$event_twitter,
				"cost"=>$event_cost,
				"address"=>$event_address,
				"city"=>$event_city,
				"state"=>$event_state,
				"zip"=>$event_zip,
				"start_time"=>$event_startTime,
				"end_time"=>$event_endTime,
				"event_cat_id"=>$event_cat_id
			);
			$rows['events'][] = $post;
		}

		$this->sendResponse(200, json_encode($rows));
		$stmt->close();
	}
}

// This is the first thing that gets called when this page is loaded
// Creates a new instance of the RedeemAPI class and calls the redeem method
$api = new GetEventDetailsAPI;
$api->call();
?>