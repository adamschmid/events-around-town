<?php
require_once 'config.php';
class BaseAPI {
    protected $db;
     
    // Constructor - open DB connection
    function __construct() {
        $this->db = new mysqli(DB_HOST, DB_USER, DB_PASSWORD,DB_NAME);
    }
 
    // Destructor - close DB connection
    function __destruct() {
        $this->db->close();
    }
  
    // Helper method to send a HTTP response code/message
    function sendResponse($status = 200, $body = '')
    {
        header('Access-Control-Allow-Origin: *');
        header('Content-type: application/json');
        print $body;
    }
  
    // Do we have a token?
    function checkToken()
    {
        $has_access = false;
        
        if ( isset($_COOKIE['token']) ) {
          $token = $_COOKIE["token"];
          $has_access = $this->isValid($token);
        } else if (isset($_POST["token"])) {
          $token = $_POST["token"];
          $has_access = $this->isValid($token);
        } else {
          // Normally we would fail here but hard code for testing
          $token = "T4CtN00a2aYM6fxoB4eq0L8X85J41Y8Q";
          $has_access = $this->isValid($token);
        }
      
        return $has_access;
    }
    
    // Is our token expired?
    function isValid($token) 
    {
        $is_valid = false;
        $user_token = $this->db->real_escape_string($token);
        $stmt = $this->db->prepare("SELECT expiration 
                                  FROM gp_session
                                  WHERE token like '$user_token'
                                  ORDER BY expiration DESC LIMIT 1;");

        $stmt->execute();
        $stmt->bind_result($expiration);
        $result = $stmt->fetch();

        // We found a match, now check the expiration
        if($result) {
          $dateFromDatabase = strtotime($expiration);
          $dateTwelveHoursAgo = strtotime("-12 hours");
          // Less than twelve hours old, it's valid
          if ($dateFromDatabase >= $dateTwelveHoursAgo) {
            // Less than 12 hours ago
            $is_valid = true;
          }
        }
        return $is_valid;
    }
}