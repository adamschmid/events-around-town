<?php
require_once 'BaseAPI.php';
class GetEventDetailsAPI extends BaseAPI {
	// Main method to redeem a code
	function call() {

		$this->checkToken();
		$query = "
			SELECT
				event.id,
				event.cost,
				rat.rating,
				event.category_id,
				cat.name,
			FROM gp_event event

			INNER JOIN gp_location loc
			ON event.location_id = loc.id

			INNER JOIN gp_rating rat
			ON event.id = rat.event_id

			INNER JOIN gp_category cat
			ON  event.id = event.category_id

			--WHERE $id = event.id;
			";
		$stmt = $this->db->prepare($query);
		$stmt->execute();

		/* bind result variables */
		$stmt->bind_result(
			$event_id,
			$event_cost,
			$event_rating,
			$event_cat_id,
			$category_name
		);
		$rows = array();

		/* fetch values */
		while ($stmt->fetch()) {
			$post = array(
				"cost"=>$event_cost,
				"rating"=>$event_rating,
				"eventCategoryId"=>$event_cat_id,
				"categories"=>$category_name,
			);
			$rows['events'][] = $post;
		}

		$this->sendResponse(200, json_encode($rows));
		$stmt->close();
	}
}

// This is the first thing that gets called when this page is loaded
// Creates a new instance of the RedeemAPI class and calls the redeem method
$api = new GetEventDetailsAPI;
$api->call();
?>