<?php
require_once 'BaseAPI.php';
class CreateEventAPI extends BaseAPI {
	// Main method to redeem a code
	function call () {

		// Set default timezone
		date_default_timezone_set('America/Los_Angeles');

		// Insert values into Location database to get ID
		$stmt = $this->db->prepare("INSERT INTO gp_location (street, city, state, zip) 
									VALUES ('".$_POST["street"]."', '".$_POST["city"]."', '".$_POST["state"]."', '".$_POST["zip"]."')");
		$stmt->execute();
		$stmt->close();

		// Set inserted id as id
		$id = $this->db->insert_id;
		
		// Combine Start Date and Start Time values into one value
		$start_date = $_POST['startDate'];
		$start_time = $_POST['startTime'];

		$start_date_and_time = $start_date . ' ' . $start_time;
		$start_dt = strtotime($start_date_and_time);

		// Combine End Date and End Time values into on value
		$end_date = $_POST['endDate'];
		$end_time = $_POST['endTime'];

		$end_date_and_time = $end_date . ' ' . $end_time;
		$end_dt = strtotime($end_date_and_time);	

		// Insert values into Event database
		$stmt = $this->db->prepare("INSERT INTO gp_event (start_time, end_time, cost, event_name, website, twitter, location_id) 
									VALUES ('".date( 'Y-m-d H:i:s', $start_dt)."','".date( 'Y-m-d H:i:s', $end_dt)."','".$_POST["cost"]."','".$_POST["eventName"]."','".$_POST["website"]."','".$_POST["twitter"]."','".$id."')");
		$stmt->execute();

		$this->sendResponse(200, json_encode($rows));
		$stmt->close();


	}
}

//This is the first thing to that gets called when this page is loaded
//Creates a new instance of the RedeemAPI class and calls the redeem method
$api = new CreateEventAPI;
$api->call();
?>